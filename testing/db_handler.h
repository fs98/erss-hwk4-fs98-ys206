#include <iostream>
#include <pqxx/pqxx>
#include <string>
#include <vector>
#include <sstream>
#include "test_control.h"
#include <math.h>
//#include "global.h"

using namespace std;
using namespace pqxx;

//simple functions for formating things
string to_str(int &n);
string to_str(long &n);
string to_str(double &n);
int to_int(string str);
double to_double(string str);

//create a new account,success:account_id,duplicate:0,fail:-1
int add_account(connection *C,long account_id, double balance);

//create a new share success:share num,fail:-1
int add_share(connection *C,long account_id,string name, long num);

//create a new record, success:0,fail:-1
int add_record(connection *C, long trade_id, long num, double price, long time);

//create a new trade, sell or buy,success:trade_id,fail:-1;not exist/enough:-2
int add_trade(connection *C, string sym, long account_id, long num,  double price);

//delete a trade, fail -1 success return the deleted account_id,not exist return 0
int delete_trade(connection *C, long trade_id,string &sym,double &price);

//delete a share, fail -1 success return the deleted amount,not exist return 0
int delete_share(connection *C, long account_id,string sym);

//update share (used when cancel or sell),success:return the amount left,fail:return 0
int update_share(connection *C, long account_id,string sym,long num);

//update the account, money is the money spent, fail -1, not enough 0, success left balance
double update_account(connection *C,long account_id,double money);

//update the trade, num is the num of sym that is closed, fail -1 success:
int update_trade(connection *C, long trade_id,long num);

//get the record with the trade_id, if trade is not finish, the first vector<string> contains the left num
vector<vector<string> > query_record(connection *C,  long trade_id);
//get the trade, top is the number of trade to get, price is the given price (can be positive or negative
vector<vector<string> > query_trade(connection *C, string sym,int top, double price);


