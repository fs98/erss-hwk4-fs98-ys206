#ifndef __TEST_CONTROL_H__
#define __TEST_CONTROL_H__

#define DEBUG		0 // for printing debug info
#define CACHE           1 // use to decide whether use cache, but at present is not useful. set as 1 always
#define TIME            1 // decide whether time the program
#define LOCAL           0 // decide whether test at local or use docker
#define DOCKER          1
#define TRHEAD          50 //decide how many threads will be created. if 0, the program will decide itself based on the core numbers of the environment

#endif
