#! /bin/bash

NUM_CLIENTS=$1

for (( i=0; i<$NUM_CLIENTS; i++ ))
do
    ./client &
done

wait
