//
// Created by 孙方媛 on 3/27/18.
//

#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include "xmlcreate.h"

using namespace std;


int make_connection(int & socket_fd, struct addrinfo &host_info, struct addrinfo * host_info_list){
    int status;
    char myhostname[256];
    gethostname(myhostname,sizeof(myhostname));
    
    const char *hostname = myhostname;
    const char *port     = "12345";

    memset(&host_info, 0, sizeof(host_info));
    host_info.ai_family   = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(hostname, port, &host_info, &host_info_list);
    if (status != 0) {
        cerr << "Error: cannot get address info for host" << endl;
        cerr << "  (" << hostname << "," << port << ")" << endl;
        return -1;
    } //if

    socket_fd = socket(host_info_list->ai_family,
                       host_info_list->ai_socktype,
                       host_info_list->ai_protocol);
    if (socket_fd == -1) {
        cerr << "Error: cannot create socket" << endl;
        cerr << "  (" << hostname << "," << port << ")" << endl;
        return -1;
    } //if

    status = connect(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1) {
        cerr << "Error: cannot connect to socket" << endl;
        cerr << "  (" << hostname << "," << port << ")" << endl;
        return -1;
    } //if
    return socket_fd;
}

int main(int argc, char *argv[]) {
    int socket_fd;
    struct addrinfo host_info;
    struct addrinfo *host_info_list = NULL;

    int socket_fd_t;
    struct addrinfo host_info_t;
    struct addrinfo *host_info_list_t = NULL;


    make_connection(socket_fd, host_info, host_info_list);

    std::string xml_s_c = myxmlcreator.XMLcreator(3,8,5,1000,100000);
    //cout << xml_s_t << endl;
    const char *message_c = xml_s_c.c_str();
    //cout << "before send to server\n";
    send(socket_fd, message_c, strlen(message_c), 0);
 
    freeaddrinfo(host_info_list);
    close(socket_fd);

    make_connection(socket_fd_t, host_info_t, host_info_list_t);
    std::string xml_s_t = myxmlcreator.XMLtransaction(15,100,1000);
    //cout << xml_s_t << endl;
    const char *message_t = xml_s_t.c_str();
    //cout << "before send to server\n";
    send(socket_fd, message_t, strlen(message_t), 0);
    freeaddrinfo(host_info_list);
    close(socket_fd);
      
    

    freeaddrinfo(host_info_list_t);
    close(socket_fd);
    cout<<"fin"<<endl;
    return 0;
}
