#include "xmlcreate.h"
#include "db_handler.h"

xmlcreate myxmlcreator;
double xmlcreate::generateGaussianNoise(double mu, double sigma){
  const double epsilon = std::numeric_limits<double>::min();
  const double two_pi = 2.0*3.14159265358979323846;

  static double z0, z1;
  static bool generate;
  generate = !generate;

  if (!generate)
    return z1 * sigma + mu;

  double u1, u2;
  do
    {
      u1 = rand() * (1.0 / RAND_MAX);
      u2 = rand() * (1.0 / RAND_MAX);
    }
  while ( u1 <= epsilon );

  z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
  z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
  return z0 * sigma + mu;
}
int xmlcreate::id_create(int num){
  int i =0;
  int res = 0;
  while(num){
    i++;
    num = num/10;
  }
  for(;i>0;--i){
    res = res*10;
    res+=rand()%10;
  }
  return res;
}

string xmlcreate::XMLcreator(int a_num,int s_num, int trade_num,double average, double limit){
  string res;
  int s_start = share_num;
  int a_start = account_id.size();
  int account_num = account_id.size()+a_num;
  share_num += s_num;
  //vector<string> share;
  ptree root,create,info;
  for (int i = 0; i<a_num;++i){
    account_id.push_back(id_create(account_num));
  }
  for(int i = a_start; i<account_num;i++){
    double balance = generateGaussianNoise(average, average/1.1);
    while(balance>limit||balance<0)
      balance = generateGaussianNoise(average, average/1.1);
    account.insert(std::pair<int,int>(account_id[i],balance));
    info.put("<xmlattr>.id",account_id[i]);
    info.put("<xmlattr>.balance",balance);
    create.add_child("account",info);
    info.clear();
  }
  for(int i = a_start; i<account_num;i++){
    int share = rand()%share_num;
    auto it = account.find(account_id[i]);
    if(it!=account.end()){
      double balance = it->second;
      unordered_map<int,int> own;
      int amount = rand()%(int)(balance/10);
      if(amount){
	own.insert(std::pair<int,int>(share,amount));
	info.put("account",amount);
	sym.insert(std::pair<int,unordered_map<int,int> >(account_id[i],own));
	info.put("account.<xmlattr>.id",account_id[i]);
	info.put("<xmlattr>.sym",share);
	create.add_child("symbol",info);
	info.clear();
      }
    }
  }
  
  root.add_child("create",create);
  boost::property_tree::xml_writer_settings<string> settings('\t',1);
  stringstream ss;
  write_xml(ss,root,settings);
  string temp;
  temp = ss.str();
  res+=to_string(temp.size());
  res+="\n";
  res+=temp;
  //ss.write(res);
  return res;
}

string xmlcreate::XMLtransaction( int trade_num,double average, double limit){
  string res;
  int n;
  int o_start = order_num;
  order_num += trade_num;
  ptree root,transaction,info;
  for(int i = 0; i < trade_num; i++){
    double price = generateGaussianNoise(average, average/1.1);
    while(price>limit||price<0)
      price = generateGaussianNoise(average, average/1.1);
    n = rand()%(account.size());
    int dice = rand()%5;
    int dice2 = rand()%5;
    if(dice <2){//sell
      auto iter = sym.find(account_id[n]);
      auto it = iter->second.begin();
      int symbol = it->first;
      int amount = rand()%it->second;
      info.put("<xmlattr>.sym",symbol);
      info.put("<xmlattr>.amount",-amount);
      info.put("<xmlattr>.limit",price);
      transaction.add_child("order",info);
      info.clear();
      if(dice2<3){
	int cid = o_start+rand()%(i+1);
	if(cid){
	  info.put("<xmlattr>.id",cid);
	  transaction.add_child("cancel",info);
	  info.clear();
	}
      }
      continue;
    }
    else{//buy
      int symbol = rand()%share_num;
      int amount = rand()%100;
      info.put("<xmlattr>.sym",symbol);
      info.put("<xmlattr>.amount",amount);
      info.put("<xmlattr>.limit",price);
      transaction.add_child("order",info);
      info.clear();
      if(dice2<3){
	int cid = o_start+rand()%(i+1);
	if(cid){
	  info.put("<xmlattr>.id",cid);
	  transaction.add_child("cancel",info);
	  info.clear();
	}
      }
      continue;
    }
  }
  int qid = 1+rand()%order_num;
  info.put("<xmlattr>.id",qid);
  transaction.add_child("query",info);
  info.clear();
  root.add_child("transaction",transaction);
  root.put("transaction.<xmlattr>.id",account_id[n]);
  boost::property_tree::xml_writer_settings<string> settings('\t',1);
  stringstream ss;
  write_xml(ss,root,settings);
  string temp;
  temp = ss.str();
  res+=to_string(temp.size());
  res+="\n";
  res+=temp;
  return res;
  
}

/*int main(){
  string str1 = XMLcreator(10,3,5,1000,100000);
  cout<<str1<<endl;
  string str2 = XMLtransaction(3,100,1000);
  cout<<str2<<endl;
  return 0;
}*/
