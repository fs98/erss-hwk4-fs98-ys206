#include "database.h"
#define TR 1
#if CACHE
cache mycache;
#endif
struct timespec start_time, end_time;
struct timespec inner_start_time, inner_end_time;
double create_account=0;
double create_share=0;
double transaction_order=0;
double transaction_match=0;
double transaction_query=0;
double transaction_cancel=0;
void matching(connection *C,ptree pt,ptree &result){
#if TIME
  clock_gettime(CLOCK_MONOTONIC, &inner_start_time);
#endif
  auto item= pt.get_child_optional("match");
  if(!item){
#if DEBUG
    cout<<"nothing is matched"<<endl;
    
#endif
    return;
  }
  BOOST_FOREACH(ptree::value_type &v1, pt.get_child("match")){
    ptree info;
    BOOST_FOREACH(ptree::value_type &v2, v1.second){
      if(v2.first == "update"||v2.first == "update_mp"){
	int trade_id = v2.second.get<int>("<xmlattr>.trade_id");
	int account = v2.second.get<int>("<xmlattr>.account_id");
	double limit = v2.second.get<double>("<xmlattr>.deal_price");
	int amount = v2.second.get<int>("<xmlattr>.amount");
	string sym = v2.second.get<string>("<xmlattr>.sym");
#if DEBUG
	cout<<"update:"<<trade_id<<" "<<amount<<endl;
#endif
	int ut=update_trade(C,trade_id,amount);
#if DEBUG
	cout<<"update trade: "<<ut<<endl;
#endif

	if(ut==-1){
	  info.put("<xmlattr>.id",trade_id);
	  info.put("","Database error");
	  result.add_child("error",info);
	  info.clear();
	  continue;
	}
	else{
	  if(amount>0){//buy
	    int as = add_share(C,account,sym,amount);
#if DEBUG
	    cout<<"update buy:add_share "<<as<<endl;
#endif
	    if(as<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }
	    time_t rawtime;
	    int current = time(&rawtime);
	    int ar=add_record(C,trade_id,amount,limit,current);
	    if(ar<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }
	  }
	  else{//sell
	    /*
	    int as = add_share(C,account,sym,amount);
#if DEBUG
	    cout<<"update sell:add_share "<<as<<endl;
#endif

	    if(as<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }
	    */
	    time_t rawtime;
	    int current = time(&rawtime);	    
	    int ar=add_record(C,trade_id,amount,limit,current);
	    if(ar<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }
	    int ua=update_account(C,account,((double)amount)*limit);
#if DEBUG
	    cout<<"update sell:update account "<<ua<<endl;
#endif

	    if(ua<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when update account");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    } 
	  }
	}
      }
      if(v2.first == "delete"||v2.first == "delete_mp"){
	int trade_id = v2.second.get<int>("<xmlattr>.trade_id");
	int account = v2.second.get<int>("<xmlattr>.account_id");
	int amount = v2.second.get<int>("<xmlattr>.amount");
	double limit = v2.second.get<double>("<xmlattr>.deal_price");
	string sym = v2.second.get<string>("<xmlattr>.sym");
	string ts;
	double pr;
	int dt = delete_trade(C,trade_id,ts,pr);
#if DEBUG
	    cout<<"delete : "<<dt<<endl;
#endif

	if(dt==-1){
	  info.put("<xmlattr>.id",trade_id);
	  info.put("","Database error when delete trade");
	  result.add_child("error",info);
	  info.clear();
	  continue;
	}
	else{
	  if(amount>0){//buy
	    int as = add_share(C,account,sym,amount);
#if DEBUG
	    cout<<"update share buy:add_share "<<as<<endl;
#endif
	    
	    if(as<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }
	    time_t rawtime;
	    int current = time(&rawtime);
	    int ar=add_record(C,trade_id,amount,limit,current);
	    if(ar<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }

	  }
	  else{//sell
	    
	    time_t rawtime;
	    int current = time(&rawtime);	    
	    int ar=add_record(C,trade_id,amount,limit,current);
	    if(ar<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when add share");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    }
	    int ua=update_account(C,account,((double)amount)*limit);
	    if(ua<0){
	      info.put("<xmlattr>.id",trade_id);
	      info.put("","Database error when update account");
	      result.add_child("error",info);
	      info.clear();
	      continue;
	    } 
	  } 
	}	  
      }
    }
  }
#if TIME
    clock_gettime(CLOCK_MONOTONIC, &inner_end_time);
    double mt = calc_time(inner_start_time,inner_end_time);
    transaction_match += mt;
#endif
  //root.add_child("results",result);
}
ptree operate_create(connection *C,ptree pt){
  ptree root,result;
  BOOST_FOREACH(ptree::value_type &v1, pt.get_child("create")){
    ptree info;
    
    if(v1.first == "account"){
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &start_time);
#endif
      int id = v1.second.get<int>("<xmlattr>.id");
      double balance = v1.second.get<double>("<xmlattr>.balance");
#if DEBUG
      cout << "new account id= " << v1.second.get<string>("<xmlattr>.id") << endl;
      cout << "create account" << endl;
      cout << "id: " << id << endl;
      cout << "balance: " << balance << endl;
#endif
      
      int r = add_account(C,id,balance);
      if(r==0){
	info.put("<xmlattr>.id",id);
	info.put("","Account already exists");
	result.add_child("error",info);
      }
      else if(r<0){
	info.put("<xmlattr>.id",id);
	info.put("","Database error");
	result.add_child("error",info);
      }
      else{
	info.put("<xmlattr>.id",id);
	result.add_child("created",info);
      }
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &end_time);
      double cat = calc_time(start_time,end_time);
      create_account += cat;
#endif
    }

    else if(v1.first == "symbol"){
#if TIME
    clock_gettime(CLOCK_MONOTONIC, &start_time);
#endif
      std::string type = v1.second.get<string>("<xmlattr>.sym");
#if DEBUG
      cout << "type: " << type << endl;
#endif
      int id;
      int num;
      BOOST_FOREACH(ptree::value_type &v2, v1.second){
	if(v2.first != "<xmlattr>"){
	  id = v2.second.get<int>("<xmlattr>.id");
	  num = to_int(v2.second.data());
#if DEBUG
	  cout << "id: " << id << endl;
	  cout << "nums: " << num << endl;
#endif
	  int r = add_share(C,id,type,num);
	  if(r<0){
	    info.put("<xmlattr>.sym",type);
	    info.put("","Database error");
	    result.add_child("error",info);
	  }
	  else{
	    info.put("<xmlattr>.id",id);
	    result.add_child("created",info);
	  }
	}
      }      
    }
#if TIME
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    double cst = calc_time(start_time,end_time);
    create_share += cst;
#endif
  }
  root.add_child("Results",result);
  return root;
}

ptree operate_transaction(connection *C,ptree pt){
  ptree root,result;
#if DEBUG
      cout<<"transaction"<<endl;
#endif

  long account_id = pt.get<long>("transaction.<xmlattr>.id");
  //cout<<"transaction: id="<<account_id<<endl;
  BOOST_FOREACH(ptree::value_type &v1, pt.get_child("transaction")){
    ptree info,status;
    if(v1.first == "order"){
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &start_time);
#endif
      string sym = v1.second.get<string>("<xmlattr>.sym");
      int amount = v1.second.get<int>("<xmlattr>.amount");
      double limit = v1.second.get<double>("<xmlattr>.limit");
#if DEBUG
      cout << "new order:"<<endl;
      cout << "sym: " << sym<< endl;
      cout << "amount: " << amount << endl;
      cout << "limit: " << limit << endl<<endl;
#endif
      info.put("<xmlattr>.sym",sym);
      info.put("<xmlattr>.amount",amount);
      info.put("<xmlattr>.limit",limit);
      if(amount>0){
	int ua=update_account(C,account_id,((double)amount)*limit);
	
	if(ua==-1){ //here has a prob that the ua and at should be done at the same
	  info.put("","Database error");
	  result.add_child("error",info);
	  info.clear();
	  continue;
	}
	if(ua==-2){
	  info.put("","Balance is not enough");
	  result.add_child("error",info);
	  info.clear();
	  continue;
	}
	if(ua==0){
	  info.put("","Account does not exist");
	  result.add_child("error",info);
	  info.clear();
	  continue;
	}
      }
      int at=add_trade(C,sym,account_id,amount,limit);
      //cout<<"add_trade: "<<at<<endl;
      if(at==-1){
	info.put("","Databa se error");
	result.add_child("error",info);
	info.clear();
	continue;
      }
      if(at==-2){
	info.put("","The amount of share is not enough");
	result.add_child("error",info);
	info.clear();
	continue;
      }
      if(at>0){
	//cout<<"at>0"<<endl;
	info.put("<xmlattr>.id",at);
	result.add_child("opened",info);
	info.clear();
	
#if CACHE
	
	//mycache.add(sym,at,amount,limit,account_id);
	ptree match_res=mycache.match(account_id,at,sym,amount,limit);
        matching(C,match_res,result);
	
#endif
#if DEBUG
	
	std::stringstream ss;
	boost::property_tree::xml_writer_settings<std::string> settings('\t',1);
	write_xml(ss,match_res,settings);
	std::cout << ss.str() << '\n';
	cout<<"match_over"<<endl;
	
#endif

      }
#if TIME
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    double tr = calc_time(start_time,end_time);
    transaction_order += tr;
#endif

    }
    //handle query
    else if(v1.first == "query"){
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &start_time);
#endif
      int id = v1.second.get<int>("<xmlattr>.id");
#if DEBUG
      cout << "Query: "<<endl<<"id:" << id << endl;
#endif
      vector<vector<string> > r = query_record(C,id);
      if(!r[0][0].compare("-1")){
	info.put("<xmlattr>.id",id);
	info.put("","Database error");
	result.add_child("error",info);
	info.clear();
	continue;
      }
      else{
	status.put("<xmlattr>.id",id);
	if(r[0][0].compare("-2")){
	  info.put("<xmlattr>.shares",r[0][0]);
	  status.add_child("open",info);
	  info.clear();
	}
	else{
	  if(r.size()<2){
	    //cout<<"not exist"<<endl;
	    info.put("<xmlattr>.id",id);
	    info.put("","Order ID to query does not exist");
	    result.add_child("error",info);
	    info.clear();
	    continue;
	  }
	}
	if(r.size()>1){
	  if(!r[1][0].compare("-1")){
	    info.put("<xmlattr>.id",id);
	    info.put("","Database error");
	    result.add_child("error",info);
	    info.clear();
	    continue;
	  }
	  for(int i = 1; i <r.size();i++){	    
	    info.put("<xmlattr>.shares",r[i][0]);
	    info.put("<xmlattr>.price",r[i][1]);
	    info.put("<xmlattr>.time",r[i][2]);
	    if(to_double(r[i][1])>0)
	      status.add_child("executed",info);
	    else
	      status.add_child("canceled",info);
	    info.clear();	  
	  }
	}
        
	result.add_child("status",status);
      }
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &end_time);
      double tq = calc_time(start_time,end_time);
      transaction_query += tq;
#endif
    }
    
    //handle cancel
    else if(v1.first == "cancel"){
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &start_time);
#endif
      int id = v1.second.get<int>("<xmlattr>.id");
#if DEBUG
      cout << "cancel"<<endl<<"id: " << id << endl << endl;
#endif
      string sym;
      double price;
      int num = delete_trade(C,id,sym,price);
#if DEBUG
      cout<<"num"<<num<<endl;
#endif
#if DEBUG
      cout<<"sym"<<sym<<endl;
#endif
      info.put("<xmlattr>.id",id);
      if(num>0){ //>0 means a buy trade
	double money = ((double)num-1)*price;
	int up=update_account(C,account_id,-money);
#if DEBUG
	cout<<"up"<<up<<endl;
#endif
	if(up>0){
	  time_t rawtime;
	  int current = time(&rawtime);
	  add_record(C,id,num,price,current);//may have prob
	  result.add_child("canceled",info);
	}
	else if(up == 0){
	  info.put("","Account does not own this order");
	  result.add_child("error",info);
	}
	else{
	  info.put("","Database error");
	  result.add_child("error",info);
	}
      }
      else if(num ==0){
	info.put("","Order ID to cancel does not exist");
	result.add_child("error",info);
      }
      else{//sell
	int ad= add_share(C,account_id,sym,num);
	if(ad>0){
	  result.add_child("canceled",info);
	}
	else{
	  info.put("","Database error");
	  result.add_child("error",info);
	}
	info.clear();
      }
#if TIME
      clock_gettime(CLOCK_MONOTONIC, &end_time);
      double tc = calc_time(start_time,end_time);
      transaction_cancel += tc;
#endif
    }
    status.clear();
  }
  root.add_child("results",result);
  return root;
}
void outputtime(){
  cout<<"create_account= "<< create_account / 1e9 <<" seconds\n";
  cout<<"create_share= "<< create_share / 1e9 <<" seconds\n";
  cout<<"transaction_order= "<< transaction_order / 1e9 <<" seconds\n";  
  cout<<"transaction_match= "<< transaction_match / 1e9 <<" seconds\n";
  cout<<"transaction_query= "<< transaction_query / 1e9 <<" seconds\n";
  cout<<"transaction_cancel= "<< transaction_cancel / 1e9 <<" seconds\n";
  cout<<"create_account= "<< create_account / 1e9 <<" seconds\n";
  create_account=0;
  create_share=0;
  transaction_order=0;
  transaction_match=0;
  transaction_query=0;
  transaction_cancel=0;
}
