#include "db_handler.h"
string format(string str){
    str.insert(0,"'");    
    for(int i = 1 ; i<str.size();i++){
      if(str[i] == '\''){
	str.insert(i,"'");
	i++;
      }
    }
    str+="'";
  str+=",";
  return str;
}
string to_str(int &n){
  stringstream ss;
  ss<<n;
  string str;
  ss>>str;
  return str;
}
string to_str(long &n){
  stringstream ss;
  ss<<n;
  string str;
  ss>>str;
  return str;
}
string to_str(double &n){
  stringstream ss;
  ss<<n;
  string str;
  ss>>str;
  return str;
}
int to_int(string str){
  stringstream ss;
  ss<<str;
  int n;
  ss>>n;
  return n;
}
double to_double(string str){
  stringstream ss;
  ss<<str;
  double n;
  ss>>n;
  return n;
}
int add_account(connection *C,long account_id,double balance){
  string command ="INSERT INTO ACCOUNT (ACCOUNT_ID,BALANCE) VALUES (";
  command += format(to_str(account_id));
  command += to_str(balance);
  command +=") ON CONFLICT(ACCOUNT_ID) DO NOTHING RETURNING ID;";
  result r;
  //cout << command << endl;
  try{
    work W(*C);
    
    r=W.exec(command.c_str());
    
    W.commit();
    
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"add_account: "<< e.what() <<std::endl;
#endif
    return -1;
  }
  if(r.size())
    return to_int(r[0][0].c_str());
  else
    return 0;
}

double check_balance(connection *C,long account_id){
  string command = "SELECT BALANCE FROM ACCOUNT WHERE ACCOUNT_ID=";
  command += to_string(account_id);
  command +="FOR UPDATE;";
  result r;
  try{
    printf("%p", C);
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
    if(r.size()>0){
      return to_double(r[0][0].c_str());
    }
    else{
      return -2;//not exist
    }
  }catch(const std::exception &e){
#if DEBUG
    cerr<<"check_balance:"<<e.what()<<endl;
#endif
    return -1;
  }

}
int add_share(connection *C,long account_id,string name, long num){
  string command;
  command += "INSERT INTO SHARE (ACCOUNT_ID,NAME,NUM) VALUES (";
  command += format(to_string(account_id));
  command += format(name);
  command += to_str(num);
  command +=") ON CONFLICT(ACCOUNT_ID,NAME) DO UPDATE SET NUM=SHARE.NUM+";
  command += to_str(num);
  command +=" WHERE SHARE.ACCOUNT_ID=";
  command += to_string(account_id);
  command += " AND SHARE.NAME=";
  command += format(name);
  command.pop_back();
  command +=" RETURNING NUM;";
  result r;
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    
#if DEBUG
    cerr <<"add_share: "<< e.what() <<std::endl;
#endif
    return -1;
  }
  return abs(to_int(r[0][0].c_str()));
}


int check_share(connection *C,long account_id,string sym){
  string command = "SELECT NUM FROM SHARE WHERE ACCOUNT_ID=";
  command += to_string(account_id);
  command +=" AND NAME=";
  command +=format(to_string(sym));
  command.pop_back();
  command +="FOR UPDATE;";
  result r;
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
    if(r.size()>0){
      return to_int(r[0][0].c_str());
    }
    else{
      return -2;
    }
  }catch(const std::exception &e){
#if DEBUG
    cerr<<"check_share:"<<e.what()<<endl;
#endif
    return -1;
  }
}

int add_record(connection *C,  long trade_id, long num, double price, long time){
  string command = "INSERT INTO RECORD (RECORD_ID,TRADE_ID,NUM,PRICE,TIME)"
    "VALUES ((SELECT CASE WHEN MAX(RECORD_ID) IS NULL THEN 1 ELSE MAX(RECORD_ID)+1 END FROM RECORD),";
  command += format(to_str(trade_id));
  //command += format(sym);
  command += format(to_str(num));
  command += format(to_str(price));
  command += to_str(time);
  command +=");";
  try{
    work W(*C);
    W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"add_record: "<< e.what() <<std::endl;
#endif
    return -1;
  }
  return 0;
}

int add_trade(connection *C, string sym, long account_id, long num,  double price){
  if(num<0){
    int i = check_share(C,account_id,sym);
    if(i==-1){ //problem happen
#if DEBUG
      cout<<"add_trade:problem happens"<<endl;
#endif
      return -1;
    }
    if(i==-2){ //not exist
#if DEBUG
      cout<<"add_trade:problem happens"<<endl;
#endif
      return -2;
    }
    
    if(i<num){ //not enough
#if DEBUG
      cout<<"add_trade:duplicate"<<endl;
#endif
      return 0;
    }
    cout<<"update_share: "<<account_id<<" "<<sym<<endl;
    int us = update_share(C,account_id,sym,-num);
    cout<<"update_share: "<<account_id<<" "<<sym<<endl;
    if(us<0)
      return -1;
  }
  string command = "INSERT INTO TRADE (SYM,ACCOUNT_ID,LEFT_NUM,PRICE)"
    "VALUES (";
  command += format(sym);
  command += format(to_str(account_id));
  command += format(to_str(num));
  command += to_str(price);
  command +=") RETURNING TRADE_ID;";
  result r;
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"add_trade: "<< e.what() <<std::endl;
    return -1;
#endif
  }
  return to_int(r[0][0].c_str());
}

int delete_trade(connection *C, long trade_id,string &sym,double &price){
  string command = "DELETE FROM TRADE WHERE TRADE_ID=";
  command += to_str(trade_id);
  command += " RETURNING LEFT_NUM,SYM,PRICE;";
  result r;
#if DEBUG
  cout<<"deletetrade:"<<endl<<command<<endl;
#endif
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
    if(r.size()){
      sym += r[0][1].c_str();
      price = to_double(r[0][2].c_str());
      
      int res =to_int(r[0][0].c_str());
      if(res>=0)
	return res+1;
      if(res<0)
	return res-1;
    }
    else{
      return 0;
    }
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"delete_trade: "<< e.what() <<std::endl;
#endif
    return -1;
  }
  
  
}
int delete_share(connection *C, long account_id,string sym){
  string command = "DELETE FROM SHARE WHERE ACCOUNT_ID=";
  command += to_str(account_id);
  command += " AND NAME=";
  command += format(sym);
  command.pop_back();
  command +=" RETURNING NUM;";
  result r;
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
    if(r.size()){
      return to_int(r[0][0].c_str());
    }
    else{
      return 0;
    }
  }catch(const std::exception &e){
    cerr <<"delete_share: "<< e.what() <<std::endl;
    return 0;
  }
  
}

double update_account(connection *C,long account_id,double money){
  double balance = check_balance(C,account_id);
  if(balance == -1){
#if DEBUG
    cout<<"error"<<endl;
#endif
    return -1; // error
  }
  if(balance == -2){
#if DEBUG
    cout<<"not exist"<<endl;
#endif
    return 0; // not exist
  }
  if(money>0){//want to buy
    if(balance<(-money)){
#if DEBUG
      cout<<"not enough"<<endl;
#endif
      return -2; // not enough
    }
  }
  string command = "UPDATE ACCOUNT SET BALANCE=BALANCE";
  if(money>=0) //buy
    command+="-";
  else{
    money = -money;
    command+="+";
  }
  command += to_str(money);
  command+= " WHERE ACCOUNT_ID=";
  command += to_str(account_id);
  command +=";";
  result r;
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"update_account: "<< e.what() <<std::endl;
    
  }
  return balance+money+1.0;
}

int update_share(connection *C,long account_id,string sym,long num){
  
  string command = "UPDATE SHARE SET NUM=NUM-";
  command += to_str(num);
  command += " WHERE ACCOUNT_ID=";
  command += to_str(account_id);
  command += " AND NAME=";
  command += format(sym);
  command.pop_back();
  command +=" RETURNING NUM;";
  result r;
  
  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"update_share: "<< e.what() <<std::endl;
    return -1;
  }
  

  int rest = to_int(r[0][0].c_str());
  if(rest)
    return to_int(r[0][0].c_str());
  else{
    delete_share(C,account_id,sym);
    return 0;
  }
}

int update_trade(connection *C, long trade_id,long num){
  string command = "UPDATE TRADE SET LEFT_NUM=LEFT_NUM";
  if(num>0){
    command+="-";
  }
  else{
    command+="+";
    num = -num;
  }
  command += to_str(num);
  command += " WHERE TRADE_ID=";
  command += to_str(trade_id);
  command +=" RETURNING LEFT_NUM;";
  result r;
#if DEBUG
  cout <<"update_trade: "<<endl<<command <<std::endl;
#endif

  try{
    work W(*C);
    r=W.exec(command.c_str());
    W.commit();
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"update_trade: "<< e.what() <<std::endl;
#endif
    return -1;
  }
  int res =to_int(r[0][0].c_str());
  if(res>=0)
    return res;
  if(res<0)
    return res-1;
}

vector<string> query_left(connection *C,long trade_id){
  string command = "SELECT LEFT_NUM FROM TRADE WHERE TRADE_ID=";
  command+=to_str(trade_id);
  command+="FOR UPDATE;";
  vector<string> res;
  try{
    work W(*C);
    result r=W.exec(command.c_str());
    W.commit();
    int rows = 0,columns= 0;
    rows = r.size();
    if(rows){
      columns = r[0].size();	
      for(int i = 0; i < rows; i++){
	for(int j = 0; j<columns; j++){
	  res.push_back(r[i][j].c_str());
	}
      }
    }
    else{
      res.push_back("-2");
    }
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"query_left: "<< e.what() <<std::endl;
#endif
    res.push_back("-1"); //error
    return res;
  }
  return res;
}


vector<vector<string> > query_record(connection *C, long trade_id){
  string command = "SELECT NUM,PRICE,TIME FROM RECORD WHERE TRADE_ID=";
  command+=to_str(trade_id);
  command+="FOR UPDATE;";
  vector<vector<string> >res;
  vector<string> left = query_left(C,trade_id);
  try{
    work W(*C);
    result r = W.exec(command.c_str());
    W.commit();
    int rows = 0,columns= 0;
    rows = r.size();
    if(rows){
      columns = r[0].size();
      res.resize(rows);
      for(int i = 0; i < rows; i++){
	for(int j = 0; j<columns; j++){
	  res[i].push_back(r[i][j].c_str());
	}
      }
    }
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"query_record: "<< e.what() <<std::endl;
#endif
    res.resize(1);
    res[0].push_back("-1");
    return res;
  }
  res.insert(res.begin(),left);
  return res;
}

vector<vector<string> > query_trade(connection *C, string sym,int top, double price){
  string command = "SELECT * FROM TRADE WHERE TRADE_ID IN";
  command +=" (SELECT TRADE_ID FROM TRADE WHERE SYM=";
  command +=format(sym);
  command.pop_back();
  command +=" AND PRICE";
  if(price>=0)
    command +=">=";
  else
    command +="<=";
  command+=to_str(price);
  command+=" ORDER BY PRICE ";
  if(price>=0)
    command +="DESC,TRADE_ID ASC)";
  else
    command +="ASC,TRADE_ID ASC)";
  command +=" LIMIT ";
  command += to_str(top);
  command +=";";
  vector<vector<string> >res;
  try{
    work W(*C);
    result r = W.exec(command.c_str());
    W.commit();
    int rows = 0,columns= 0;
    rows = r.size();
    res.resize(rows);
    if(rows){
      columns = r[0].size();
      for(int i = 0; i < rows; i++){
	for(int j = 1; j<columns; j++){
	  res[i].push_back(r[i][j].c_str());
	}
      }
    }
  }catch(const std::exception &e){
#if DEBUG
    cerr <<"query_trade: "<< e.what() <<std::endl;
#endif
    //return;
  }
  
  return res;
}

