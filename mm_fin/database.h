#include "db_handler.h"
#include "cache.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <time.h>
#include <math.h>
#include "test_control.h"
#include "timer.h"

using namespace boost::property_tree;

ptree operate_create(connection *C,ptree pt);
ptree operate_transaction(connection *C,ptree pt);
void outputtime();
