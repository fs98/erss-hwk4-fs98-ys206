#include "db_handler.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>
#include <limits>
#include <unordered_map>

using namespace std;
using namespace boost::property_tree;

string XMLcreator(int account_num,int share_num, int trade_num,double average, double limit); 
string XMLtransaction(int trade_num,double average, double limit); 

