#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>
using namespace std;
double get_num(string str){
  double n;
  string s;
  stringstream ss;
  ss<<str;
  ss>>s;
  ss>>n;
  return n;
}
bool find(string s1, string s2){
  size_t f = s1.find(s2);
  if(f!=std::string::npos)
    return true;
  else
    return false;
}
int main(int argc, char ** argv){
  double create_account=0;
  double create_share=0;
  double transaction_order=0;
  double transaction_match=0;
  double transaction_query=0;
  double transaction_cancel=0;
  double operate_time=0;
  double read_time = 0;
  double write_time = 0;
  string readline;
  
  if(argc<3){
    cout<<"miss filename!"<<endl;
    return 0;
  }
  string num = argv[2];
  ifstream file;
  fstream write;
  file.open(argv[1],ios::in);
  write.open("outcome.txt",ios::out|ios::app);
  
  if(!file.is_open()){
    cout<<"fail to open"<<endl;
    return 0;
  }
  if(!write.is_open()){
    cout<<"fail to write"<<endl;
    return 0;
  }
  
  while(getline(file,readline)){
    if(find(readline,"create_account=")){
      create_account+=get_num(readline);
    }
    if(find(readline,"create_share=")){
      create_share+=get_num(readline);
    }
    if(find(readline,"transaction_order=")){
      transaction_order+=get_num(readline);
    }
    if(find(readline,"transaction_match=")){
      transaction_match+=get_num(readline);
    }
    if(find(readline,"transaction_query=")){
      transaction_query+=get_num(readline);
    }
    if(find(readline,"transaction_cancel=")){
      transaction_cancel+=get_num(readline);
    }
    if(find(readline,"operate_time=")){
      operate_time+=get_num(readline);
    }
  }
  time_t rawtime;
  int current = time(&rawtime);
  write<<ctime(&rawtime);
  write<<"--------------------------------\n";
  write<<"client num: "<<num<<"\n";
  write<<"create_account= "<< create_account <<" seconds\n";
  write<<"create_share= "<< create_share  <<" seconds\n";
  write<<"transaction_order= "<< transaction_order  <<" seconds\n";  
  write<<"transaction_match= "<< transaction_match <<" seconds\n";
  write<<"transaction_query= "<< transaction_query <<" seconds\n";
  write<<"transaction_cancel= "<< transaction_cancel  <<" seconds\n";
  write<<"operate_time= "<< operate_time <<" seconds\n";
  write<<"--------------------------------\n";
  file.close();
  write.close();
}
