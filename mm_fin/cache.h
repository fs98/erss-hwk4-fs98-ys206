//
// Created by 孙方媛 on 4/3/18.
//

#ifndef CLIONPROJECTS_CACHE_H
#define CLIONPROJECTS_CACHE_H
#include "test_control.h"
#include <queue>
#include <unordered_map>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <string>
#include <iostream>
class trade{
 public:
  trade(){}
 trade(int _trade_id, int _left_amount, double _price, int _account_id):trade_id(_trade_id), left_amount(_left_amount), price(_price)\
    , account_id(_account_id){}
  
  int trade_id;
  int left_amount;
  double price;
  int account_id;
};


class mycomparison{
  bool max;
 public:
  mycomparison(const bool& maxparam = false){
    max = maxparam;
  }
  bool operator()(const trade& lhs, const trade& rhs)const{
    if(max){
      if(lhs.price < rhs.price){
	return true;
      } else if(lhs.price == rhs.price){
	return lhs.trade_id > rhs.trade_id;
      } else{
	return false;
      }
    }
    else{
      if(lhs.price < rhs.price){
	return false;
      } else if(lhs.price == rhs.price){
	return lhs.trade_id > rhs.trade_id;
      } else{
	return false;
      }
    }
  }
};

class yourcomparison{
  bool max;
 public:
  yourcomparison(const bool& maxparam = true){
    max = maxparam;
  }
  bool operator()(const trade& lhs, const trade& rhs)const{
    if(max){
      if(lhs.price < rhs.price){
	return true;
      } else if(lhs.price == rhs.price){
	return lhs.trade_id > rhs.trade_id;
      } else{
	return false;
      }
    }
    else{
      if(lhs.price < rhs.price){
	return false;
      } else if(lhs.price == rhs.price){
	return lhs.trade_id > rhs.trade_id;
      } else{
	return false;
      }
    }
  }
};

typedef std::priority_queue<trade, std::vector<trade>, mycomparison> minheap;
typedef std::priority_queue<trade, std::vector<trade>, yourcomparison> maxheap;


class cache{
 public:
  void add(std::string sym, int trade_id, int amount, double price, int account_id);
  boost::property_tree::ptree match(int account_id, int trade_id, std::string sym, int amount, double price);
  
  std::unordered_map<std::string, maxheap> mem_trades_buy;
  std::unordered_map<std::string, minheap> mem_trades_sell;
};

extern cache mycache;
#endif //CLIONPROJECTS_CACHE_H
