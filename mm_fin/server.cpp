#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <pqxx/pqxx>
#include "taskDistributor.h"
#include "httpSock.h"
#include "global.h"
#include "test_control.h"
using namespace pqxx;
//Allocate & initialize a Postgres connection object
static unordered_map<int,double> account;
static unordered_map<int,unordered_map<int,int> >sym;//account,<sym,amount>
static vector<int> account_id;
static int share_num;

void set_up_db(){
  connection *C;
  try{
    //Establish a connection to the database
    //Parameters: database name, user name, user password
#if DOCKER
    C = new connection("dbname=match_db user=postgres password=psql "
		       "host=exchange_db port=5432");
#endif
#if LOCAL
    C = new connection("dbname=match_db user=postgres password=passw0rd ");
#endif

    if (C->is_open()) {
      cout << "Opened database successfully: " << C->dbname() << endl;
    } else {
      cout << "Can't open database" << endl;
      exit(EXIT_FAILURE);
    }
  } catch (const std::exception &e){
    cerr <<"setup connect: "<< e.what() << std::endl;
    exit(EXIT_FAILURE);
  }
  const char *sql;
  string pre=
    "DROP TABLE IF EXISTS RECORD;"
    "DROP TABLE IF EXISTS TRADE;"
    "DROP TABLE IF EXISTS SHARE;"
    "DROP TABLE IF EXISTS ACCOUNT";

  string set=
    "CREATE TABLE ACCOUNT("
    "ID SERIAL PRIMARY KEY,"
    "ACCOUNT_ID INT NOT NULL UNIQUE,"
    "BALANCE               FLOAT NOT NULL DEFAULT 0);"
    "CREATE TABLE SHARE("
    "SHARE_ID          SERIAL,"
    "ACCOUNT_ID          INT NOT NULL,"
    "NAME                TEXT NOT NULL,"
    "NUM                 INT NOT NULL,"
    "UNIQUE(ACCOUNT_ID,NAME),"
    "FOREIGN KEY (ACCOUNT_ID) REFERENCES ACCOUNT(ACCOUNT_ID));"
    "CREATE TABLE TRADE("
    "TRADE_ID           SERIAL,"
    "SYM                TEXT NOT NULL,"
    "ACCOUNT_ID          INT NOT NULL,"
    "LEFT_NUM            INT NOT NULL,"
    "PRICE             FLOAT NOT NULL,"
    "FOREIGN KEY (ACCOUNT_ID) REFERENCES ACCOUNT(ACCOUNT_ID));"
    "CREATE TABLE RECORD("
    "RECORD_ID INT PRIMARY KEY  NOT NULL,"
    "TRADE_ID            INT NOT NULL,"
    "NUM                 INT NOT NULL,"
    "PRICE             FLOAT NOT NULL,"
    "TIME                INT NOT NULL);";;
  try{
    work W(*C);
    result r = W.exec(pre.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"drop: "<< e.what() <<std::endl;
    exit(EXIT_FAILURE);
  }
  try{
    work W(*C);
    result r = W.exec(set.c_str());
    W.commit();
  }catch(const std::exception &e){
    cerr <<"create: "<< e.what() <<std::endl;
    exit(EXIT_FAILURE);
  }

  C->disconnect();
}

int main() {
  cerr <<"Matching Server setup..."<<endl;
  set_up_db();
  TaskDistributor<Accept, TaskInfo> taskMaster;
  taskMaster.getTask();
  return EXIT_SUCCESS;
}
