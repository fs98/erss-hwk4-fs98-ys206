//
// Created by 孙方媛 on 4/6/18.
//

#ifndef CLIONPROJECTS_XMLCREATE_H
#define CLIONPROJECTS_XMLCREATE_H
#include <unordered_map>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

using namespace std;
using namespace boost::property_tree;

class xmlcreate{
public:
    double generateGaussianNoise(double mu, double sigma);
    int id_create(int num);
    string XMLcreator(int a_num,int s_num, int trade_num,double average, double limit);
    string XMLtransaction( int trade_num,double average, double limit);


private:
    unordered_map<int,double> account;
    unordered_map<int,unordered_map<int,int> >sym;//account,<sym,amount>
    vector<int> account_id;
    int share_num;
    int order_num;
};

extern xmlcreate myxmlcreator;

#endif //CLIONPROJECTS_XMLCREATE_H
