#! /bin/bash

NUM_CLIENTS=$1

sudo pkill -9 server

./server > log&

sleep 2

for (( i=0; i<$NUM_CLIENTS; i++ ))
do
    ./client &
done

wait
