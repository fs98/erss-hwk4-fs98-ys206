//
// Created by 孙方媛 on 4/3/18.
//

#include "cache.h"

void cache::add(std::string sym, int trade_id, int left_amount, double price, int account_id){
  if(left_amount>0){
    //This is a buy order
      
    if(mem_trades_buy.count(sym) == 0){
      trade t = trade(trade_id, left_amount, price, account_id);
      mem_trades_buy[sym] = maxheap();
      mem_trades_buy[sym].push(t);
    }
    else{
      trade t = trade(trade_id, left_amount, price, account_id);
      mem_trades_buy[sym].push(t);
    }
  }
  else{
    if(mem_trades_sell.count(sym) == 0){
      trade t = trade(trade_id, -left_amount, price, account_id);
      mem_trades_sell[sym] = minheap();
      mem_trades_sell[sym].push(t);
    }
    else{
      trade t = trade(trade_id, -left_amount, price, account_id);
      mem_trades_sell[sym].push(t);
    }
  }
    
}


boost::property_tree::ptree cache::match(int account_id, int trade_id, std::string sym, int amount, double limit){
  boost::property_tree::ptree res;
  boost::property_tree::ptree ops;
  if(amount>0) {
    //This is a buy order, search for sell map for match
    if (mem_trades_sell.count(sym) != 0) {
      auto &heap = mem_trades_sell[sym];
      while(!heap.empty() && amount > 0){
	auto& top = heap.top();
	boost::property_tree::ptree op;
	if(top.price > limit){
	  //There is no match
#if DEBUG
	  std::cout<<"no match"<<std::endl;
#endif
	  return res;//an empty res
	} else{
	  if(top.left_amount < amount){
#if DEBUG
	    std::cout<<"buy,<"<<std::endl;
#endif
	    //res.add (delete matching order in db & update the coming order)
	    //delete a "sell"
	    op.put("delete_mp", "");
	    //int account_id = top.account_id;
	    op.put("delete_mp.<xmlattr>.account_id", top.account_id);
	    op.put("delete_mp.<xmlattr>.trade_id", top.trade_id);
	    op.put("delete_mp.<xmlattr>.sym", sym);
	    op.put("delete_mp.<xmlattr>.amount", -top.left_amount);
	    //double price = top.price;
	    op.put("delete_mp.<xmlattr>.deal_price", top.price);

	    //update the coming order, a buy
	    op.put("update", "");
	    op.put("update.<xmlattr>.sym", sym);
	    amount = amount - top.left_amount;
	    op.put("update.<xmlattr>.trade_id", trade_id);
	    op.put("update.<xmlattr>.account_id", account_id);
	    op.put("update.<xmlattr>.amount", top.left_amount);
	    op.put("update.<xmlattr>.deal_price", top.price);

	    //delete matching order in map
	    heap.pop();
	    //add the left into map, a buy
	    add(sym,trade_id,amount,limit,account_id);
	  }
	  else if(top.left_amount>amount){
#if DEBUG
	    std::cout<<"buy,>"<<std::endl;
#endif
	    //update the matching order in map
	    trade temp = trade(top.trade_id, top.left_amount-amount,
			       top.price, top.account_id);
	    //top.left_amount = top.left_amount - amount;
	    heap.pop();
	    heap.push(temp);

	    //Delete the coming order in db, a buy
	    op.put("delete", "");
	    //op.put("delete.<xmlattr>.buy_order_id", account_id);
	    op.put("delete_mp.<xmlattr>.account_id", account_id);
	    op.put("delete.<xmlattr>.trade_id", trade_id);
	    op.put("delete.<xmlattr>.sym", sym);
	    op.put("delete.<xmlattr>.amount", amount);
	    op.put("delete.<xmlattr>.deal_price", top.price);
	    //update the matched order, a sell
	    op.put("update_mp", "");
	    op.put("update_mp.<xmlattr>.sym", sym);
	    op.put("update_mp.<xmlattr>.trade_id", top.trade_id);
	    op.put("update_mp.<xmlattr>.account_id", top.account_id);
	    op.put("update_mp.<xmlattr>.amount", -amount);
	    op.put("update_mp.<xmlattr>.deal_price", top.price);
	    amount = 0;
	  }
	  else{//the same, delete both
	    op.put("delete", "");
	    //op.put("delete.<xmlattr>.buy_order_id", account_id);
	    //delete buy
	    op.put("delete.<xmlattr>.account_id", account_id);
	    op.put("delete.<xmlattr>.trade_id", trade_id);
	    op.put("delete.<xmlattr>.sym", sym);
	    op.put("delete.<xmlattr>.amount", amount);
	    op.put("delete.<xmlattr>.deal_price", limit);
	    //delete sell
	    op.put("delete_mp", "");
	    op.put("delete_mp.<xmlattr>.account_id", top.account_id);
	    op.put("delete_mp.<xmlattr>.trade_id", top.trade_id);
	    op.put("delete_mp.<xmlattr>.sym", sym);
	    op.put("delete_mp.<xmlattr>.amount", -top.left_amount);
	    op.put("delete_mp.<xmlattr>.deal_price", top.price);
	  }	    
	}
	ops.add_child("operation", op);
      }
    } else{
      //There is no same sym in map, add the coming order into map
      add(sym, trade_id, amount, limit, account_id);
      return res;
    }
  }

  else{ //a sell order
    if (mem_trades_buy.count(sym) != 0) {
      auto &heap = mem_trades_buy[sym];
      amount = -amount;
      while(!heap.empty() && amount > 0){
	auto& top = heap.top();
	boost::property_tree::ptree op;
	if(top.price < limit){
	  //There is no match
#if DEBUG
	  std::cout<<"no sell match"<<std::endl;
#endif
	  return res;//an empty res
	} else{
	  if(top.left_amount < amount){
#if DEBUG
	    std::cout<<"sell,<"<<std::endl;
#endif
	    //res.add (delete matching order in db & update the coming order)
	    //delete a buy
	    op.put("delete_mp", "");
	    //int account_id = top.account_id;
	    op.put("delete_mp.<xmlattr>.account_id", top.account_id);
	    op.put("delete_mp.<xmlattr>.trade_id", top.trade_id);
	    op.put("delete_mp.<xmlattr>.sym", sym);
	    op.put("delete_mp.<xmlattr>.amount", top.left_amount);
	    //double price = top.price;
	    op.put("delete_mp.<xmlattr>.deal_price", top.price);

	    //update the coming order, a sell
	    op.put("update", "");
	    op.put("update.<xmlattr>.sym", sym);
	    amount = amount - top.left_amount;
	    op.put("update.<xmlattr>.trade_id", trade_id);
	    op.put("update.<xmlattr>.account_id", account_id);
	    op.put("update.<xmlattr>.amount", -top.left_amount);
	    op.put("update.<xmlattr>.deal_price", top.price);

	    //delete matching order in map
	    heap.pop();
	    //add the left into map, a sell
	    add(sym,trade_id,-amount,limit,account_id);
	  }
	  else if(top.left_amount>amount){
#if DEBUG
	    std::cout<<"sell,>"<<std::endl;
#endif
	    //update the matching order in map
	    trade temp = trade(top.trade_id, top.left_amount-amount,
			       top.price, top.account_id);
	    //top.left_amount = top.left_amount - amount;
	    heap.pop();
	    heap.push(temp);

	    //Delete the coming order in db, a sell
	    op.put("delete", "");
	    op.put("delete.<xmlattr>.account_id", account_id);
	    //op.put("delete.<xmlattr>.buy_order_id", account_id);
	    op.put("delete.<xmlattr>.trade_id", trade_id);
	    op.put("delete.<xmlattr>.sym", sym);
	    op.put("delete.<xmlattr>.amount", -amount);
	    op.put("delete.<xmlattr>.deal_price", top.price);
	    //update the matched order, a buy
	    op.put("update_mp", "");
	    op.put("update_mp.<xmlattr>.sym", sym);
	    op.put("update_mp.<xmlattr>.trade_id", top.trade_id);
	    op.put("update_mp.<xmlattr>.account_id", top.account_id);
	    op.put("update_mp.<xmlattr>.amount", amount);
	    op.put("update_mp.<xmlattr>.deal_price", top.price);
	    amount = 0;
	  }
	  else{//the same, delete both
	    op.put("delete", "");
	    //op.put("delete.<xmlattr>.buy_order_id", account_id);
	    //delete sell
	    op.put("delete.<xmlattr>.account_id", account_id);
	    op.put("delete.<xmlattr>.trade_id", trade_id);
	    op.put("delete.<xmlattr>.sym", sym);
	    op.put("delete.<xmlattr>.amount", -amount);
	    op.put("delete.<xmlattr>.deal_price", top.price);
	    //delete buy
	    op.put("delete_mp", "");
	    op.put("delete_mp.<xmlattr>.account_id", top.account_id);
	    op.put("delete_mp.<xmlattr>.trade_id", top.trade_id);
	    op.put("delete_mp.<xmlattr>.sym", sym);
	    op.put("delete_mp.<xmlattr>.amount", top.left_amount);
	    op.put("delete_mp.<xmlattr>.deal_price", top.price);
	  }	    
	}
	ops.add_child("operation", op);
      }
    } else{
      //There is no same sym in map, add the coming order into map
      add(sym, trade_id, amount, limit, account_id);
      return res;
    }
    
  }
  res.put_child("match", ops);
  return res;
}
